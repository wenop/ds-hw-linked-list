# DS Homework (Link List)

## Desc.

implement a generic circular doubly linked list without a header or trailer senteniel.

Have methods

 - [x] 1 . to show current node 

 - [x] 2 . insert a node before the current node 

 - [x] 3 . insert a node after the current node 

 - [x] 4 . move the current node one place forwards or backwards 

 - [x] 5 . remove current node (delete the node) 

 - [x] 6 . return size of the list 

 - [x] 7 . return element at current node 

 - [ ] 8 . print out the entire list

 **Write a java program to** 

 - [x] a .  Instantiate the list, add 5 random Doubles.
 
 - [x] b .  Print the list
 
 - [x] c .  Find the largest value in the list and delete the node before it.
 
 - [x] d .  Print the list

_40 points = 100%_