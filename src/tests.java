import java.util.Random;

public class tests {

    public static void main(String[] argv) {
        Node n1 = new Node(2.2);
        Node n2 = new Node(2.2);

        assert n1 != n2;
        assert ! n1.equals(n2);
    }
}
