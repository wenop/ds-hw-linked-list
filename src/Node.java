import com.sun.istack.internal.Nullable;

public class Node {

    // methods like "get*Value()" return a String form of the * value

    private double content;
    private Node next;
    private Node prev;

    /**
     * Get current Node info (of the list),
     *      which is actually the **Question 1**.
     * @param howFar
     * @return
     */
    public String getNodeInfo(int howFar) {
        // todo Make it long!
        return String.format(
            "-1          |    %s \n" +
            " 0 (current)|--> %s \n" +
            "-1          |    %s \n" ,
            this.getPrevValue(),
            this.getNodeValue(),
            this.getNextValue()
        );
    }

    /**
     * Helper method to get null value tested
     * @param test node to be test
     * @return some String description
     */
    protected String getValueDesc(Node test) {
        if (test == null) {
            return "<none>";
        } else {
            return test.toString();
        }
    }

    @Override
    public String toString() {
        return String.valueOf(content);
    }

    public Node(double content) {
        this.content = content;
    }

    /*public Node(double content, Node prev, Node next) {
        this.content = content;
        this.prev = prev;
        this.next = next;
    }*/

    // ----------------------------------

    /**
     * Insert a node behind this node (this_node.next)
     * @param n node to be inserted
     */
    public void insertBehindWith(Node n) {
        if (this.next != null) {
            this.next.setPrev(null);
        }
        this.next = n;

        if (n.prev != null) {
            n.prev.setNext(null);
        }
        n.prev = this;
    }

    /**
     * Insert a node before this node (this_node.prev)
     * @param n node to be inserted
     */
    public void insertBeforeWith(Node n) {
        if (this.prev != null) {
            this.prev.setNext(null);
        }
        this.prev = n;

        if (n.next != null) {
            n.next.setPrev(null);
        }
        n.next = this;
    }

    /**
     * Insert a node behind current node
     *       node inserted returned, so that we can chained-called
     *       just make it a little bit fun
     * @param n
     * @return
     */
    public Node chain(Node n) {
        this.insertBehindWith(n);
        return n;
    }

    public void removeCurrent() {
        Node n1 = this.getNext();
        Node n2 = this.getPrev();
        if (n1 != null) {
            n1.setPrev(n2);
        }
        if (n2 != null) {
            n2.setNext(n1);
        }
    }

    /**
     * move the current node one place forwards
     */
    public void hopOneForward() {
        // todo Check NULL! (though hopefully this is a circular list)
        Node n = this.getNext();
        Node p = this.getPrev();
        Node pp = p.getPrev();

        this.setNext(p);
        this.setPrev(pp);
        pp.setNext(this);
        p.setPrev(this);
        p.setNext(n);
        n.setPrev(p);
    }

    /**
     * move the current node one place backwards
     */
    public void hopOneBackwards() {
        // well, this should be fairly simple to do with ...
        this.getNext().hopOneForward();
    }

    // ----------------------------------

    /**
     * @return return size of the list
     */
    public long getLinkedNodesCount() {
        // todo Check if the Nodes are connected circularly
        Node tmpNode = this;
        long c = 0;
        do {
            c++;
            tmpNode = tmpNode.getNext();
        } while (tmpNode != this);
        return c;
    }

    public void printAllLinkedNodes() {
        // todo Check if the Nodes are connected circularly
        Node tmpNode = this;
        do {
            System.out.println(tmpNode.getContent());
            tmpNode = tmpNode.getNext();
        } while (tmpNode != this);
    }

    // ------------------------------------

    String getNodeValue() {
        return getValueDesc(this);
    }

    public Double getContent() {
        return this.content;
    }

    @Nullable
    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    @Nullable
    public Node getPrev() {
        return prev;
    }

    public void setPrev(Node prev) {
        this.prev = prev;
    }

    public String getNextValue() {
        Node n = getNext();
        return getValueDesc(n);
    }

    public String getPrevValue() {
        Node n = getPrev();
        return getValueDesc(n);
    }
}
