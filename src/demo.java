import java.util.Random;

/**
 *
 *  Group work of
 *      [Guangxue Wen](guw16@pitt.edu)
 *      [David Guerrero](djg93@pitt.edu)
 *
 * */
public class demo {

    public static void main(String[] argv) {

        // ---- Question a: Instantiate the list, add 5 random Doubles ----
        Random random = new Random();

        Node n1 = new Node(random.nextDouble());
        Node n2 = new Node(random.nextDouble());
        Node n3 = new Node(random.nextDouble());
        Node n4 = new Node(random.nextDouble());
        Node n5 = new Node(random.nextDouble());

        n1.chain(n2).chain(n3).chain(n4).chain(n5)
                // got closed (thus get circular)
                .chain(n1);

        // ---- Question b: Print the chain (list) ----
        System.out.println("\n\n-------- whole list --------");
        // startingPoint is randomly selected
        Node startingPoint = n4;
        Node tmpNode = startingPoint;

        do {
            // just print each element
            System.out.println(tmpNode.getNodeValue());
            //System.out.println("\t\t↓");

            tmpNode = tmpNode.getNext();
        } while (tmpNode != startingPoint);

        // ---- Find the largest value in the list ...
        Node largestNode = startingPoint;
        tmpNode = startingPoint;
        do {
            if (tmpNode.getContent() > largestNode.getContent()) {
                largestNode = tmpNode;
            }

            tmpNode = tmpNode.getNext();
        } while (tmpNode != startingPoint);

        // just print the largestNode
        System.out.println("\n\n-------- largest print --------");
        tmpNode = startingPoint;
        do {
            // just print each element
            System.out.print(tmpNode.getNodeValue());
            if (tmpNode.equals(largestNode)) {
                System.out.print("\t(largest)");
            } else if (tmpNode.equals(largestNode.getPrev())) {
                System.out.print("\t(<- delete this)");
            }
            System.out.println();
            tmpNode = tmpNode.getNext();
        } while (tmpNode != startingPoint);


        // ---- ... and delete the node before it
        Node nodeToBeRemoved = largestNode.getPrev();
        nodeToBeRemoved.removeCurrent();
        if (startingPoint == nodeToBeRemoved) {
            System.out.println("startingPoint == largestNode");
            // just use another point as starting point yo
            startingPoint = startingPoint.getNext();
        }

        // just print the after delete
        System.out.println("\n\n-------- after delete --------");
        tmpNode = startingPoint;
        do {
            // just print each element
            System.out.println(tmpNode.getNodeValue());
            tmpNode = tmpNode.getNext();
        } while (tmpNode != startingPoint);
    }
}
